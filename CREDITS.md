## Credits

This application uses Open Source components. You can find the source code of
their open source projects along with license information below. We acknowledge
and are grateful to these developers for their contributions to open source.

    Project: Switch https://cruip.com/switch
    License (GPLv3.0 only)
    Usage: illustrations and base template by Pasquale Vitiello & Davide Pacilio

    Project: Overpass https://github.com/RedHatBrand/Overpass
    License (SIL Open Font License) https://github.com/RedHatBrand/Overpass/blob/master/LICENSE.md
    Usage: body font

    Project: Rajdhani https://github.com/itfoundry/rajdhani
    License (SIL Open Font License)
    Usage: headers font

    Project: unDraw https://undraw.co/
    License https://undraw.co/license
    Usage: features vector graphics