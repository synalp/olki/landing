import ScrollReveal from 'scrollreveal'
import tippy from 'tippy.js'
import GLightbox from 'glightbox'
import TypeIt from 'typeit'
const imageMapResize = require('image-map-resizer')
const store = require('store/dist/store.modern')

const doc = document
const rootEl = doc.documentElement
const body = doc.body
const lightSwitch = doc.getElementById('lights-toggle')
/* global ScrollReveal */
const sr = window.sr = ScrollReveal()

rootEl.classList.remove('no-js')
rootEl.classList.add('js')

window.addEventListener('load', function () {
  body.classList.add('is-loaded')
})

// Reveal animations
function revealAnimations () {
  sr.reveal('.feature', {
    duration: 600,
    distance: '20px',
    easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
    origin: 'right',
    viewFactor: 0.2
  })
}

if (body.classList.contains('has-animations')) {
  window.addEventListener('load', revealAnimations)
}

// Light switcher
if (lightSwitch) {
  window.addEventListener('load', checkLights)
  lightSwitch.addEventListener('change', checkLights)
}

function checkLights () {
  let labelText = lightSwitch.parentNode.querySelector('.label-text')
  if (lightSwitch.checked) {
    body.classList.remove('lights-off')
    if (labelText) {
      labelText.innerHTML = 'dark'
    }
    store.set('lights', true)
  } else {
    body.classList.add('lights-off')
    if (labelText) {
      labelText.innerHTML = 'light'
    }
    store.set('lights', false)
  }
}

if (store.get('lights') === false) {
  let labelText = lightSwitch.parentNode.querySelector('.label-text')
  body.classList.add('lights-off')
  if (labelText) {
    labelText.innerHTML = 'light'
  }
  lightSwitch.checked = false
}

tippy.setDefaults({
  placement: 'bottom'
})

imageMapResize()

GLightbox() // TODO: we use the lightbox via .glightbox on a link with href to an embed once we have a presentation video

new TypeIt('#type-effect', {
  loop: true
})
  .type('Scientific Interaction')
  .pause(3000)
  .delete()
  .type('Document Annotation')
  .pause(3000)
  .delete()
  .type('Peer Reviews')
  .pause(3000)
  .delete()
  .type('Scientific Watch')
  .pause(3000)
  .delete()
  .go()

// Dropdown menus
tippy('#aboutNav', {
  interactive: true,
  content: '<div style="text-align:left;"><a href="about.html">Editorial</a></br><a href="group-sessions.html">Group Sessions</a></br><a href="https://framagit.org/synalp/olki/olki/wikis/Frequently-Asked-Questions">F.A.Q.</a></div>',
})
